// http status is yung 200, 300, 404,
// iba rin itong HTTP Methods pero part pa din ito ng CRUD.


// ===================================

// Node.js Routing w/ HTTP methods
let http = require("http");

// wag response and request
const server = http.createServer(function (request, response) {

	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter.
	// the method "GET" means that we will be retrieving or reading information or data.
	if(request.url == "/items" && request.method == "GET"){

	// writeHead means status andd content type.
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Data retrieve from the database');
	}



	// The method "POST" means that we will be adding or creating information but for now, we will just be sending a text response for now.
		if(request.url == "/items" && request.method == "POST"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Data to be sent to the database");
	}
}).listen(4000);

console.log('Server is running at localhost:4000');

/*
mongodb: database.
robo3t: para maconfigure yung database.
postman: api, or pang,test or para lang sya f12 debugger.

*/